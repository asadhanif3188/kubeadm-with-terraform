resource "aws_instance" "kubeadm_master" {
  ami                         = "ami-0f2967bce46537146"
  instance_type               = "t3.medium"
  vpc_security_group_ids      = ["${aws_security_group.kubeadm.id}"]
  key_name                    = aws_key_pair.kubeadm_key.key_name
  associate_public_ip_address = true
  tags = {
    Name = "kubeadm-master"
  }
}

resource "null_resource" "remote_exec_master" {

  connection {
    host        = aws_instance.kubeadm_master.public_ip
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("/home/kanzal/.ssh/id_rsa")
  }


  provisioner "file" {
    source      = "master.sh"
    destination = "/tmp/kubeadm.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "set -e",
      "echo 'Waiting for cloud-init to complete...'",
      "until sudo cloud-init status --wait > /dev/null 2>&1; do sleep 1; done",
      "echo 'Completed cloud-init!'",
      "sudo bash /tmp/kubeadm.sh", # '${aws_instance.kubeadm_master.public_ip}'",
      "sleep 10",
      "exit",
    ]
  }
}


data "external" "get_token" {
  program = ["bash", "-c", <<-EOF
    until token=$(ssh -o StrictHostKeyChecking=no -i /home/kanzal/.ssh/id_rsa ubuntu@${aws_instance.kubeadm_master.public_ip} 'kubeadm token create --print-join-command'); do sleep 1; done
    echo "{\"token\":\"$token\"}"
    EOF
  ]
  depends_on = [null_resource.remote_exec_master]
}

