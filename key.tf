resource "aws_key_pair" "kubeadm_key" {
  key_name   = "kubeadm_key"
  public_key = file("/home/kanzal/.ssh/id_rsa.pub")
}
