resource "aws_instance" "kubeadm_workers" {
  count                       = var.worker_count
  ami                         = "ami-0f2967bce46537146"
  instance_type               = "t3.medium"
  vpc_security_group_ids      = ["${aws_security_group.kubeadm.id}"]
  key_name                    = aws_key_pair.kubeadm_key.key_name
  associate_public_ip_address = true
  tags = {
    Name = "k8s-worker-${count.index + 1}"
  }
}



locals {
  worker_instances = [
    for i in range(1, var.worker_count + 1) : {
      index            = i
      instance_object  = aws_instance.kubeadm_workers[i - 1]
      ssh_user         = "ubuntu"
      private_key_path = file("/home/kanzal/.ssh/id_rsa")
    }
  ]
}




resource "null_resource" "remote_exec_workers" {

  for_each = { for instance in local.worker_instances : instance.index => instance }

  depends_on = [data.external.get_token]
  connection {
    host        = each.value.instance_object.public_ip
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("/home/kanzal/.ssh/id_rsa")
  }


  provisioner "file" {
    source      = "worker.sh"
    destination = "/tmp/worker.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "set -e",
      "echo 'Waiting for cloud-init to complete...'",
      "until sudo cloud-init status --wait > /dev/null 2>&1; do sleep 1; done",
      "echo 'Completed cloud-init!'",
      "sudo bash /tmp/worker.sh '${each.value.instance_object.tags.Name}' '${data.external.get_token.result.token}'",
      "sleep 10",
      "exit",
    ]
  }
}



